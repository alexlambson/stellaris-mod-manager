from typing import List
from paradox_parser.node_object import Node
from paradox_parser import value_types
from recorder import BaseRecorder


class TextRecorder(BaseRecorder):

    @staticmethod
    def build_depth(depth: int):
        """
        Adds the appropriate number of indents for nested settings

        Parameters
        ----------
        depth : int
            The number of indents to add

        Returns
        -------
        str
            A string of whitespace indents to prefix to a settings entry
        """
        out = ""
        for _ in range(depth):
            out += "\t"
        return out

    @staticmethod
    def build_string(depth: int, formatted_string: str):
        """
        Build the settings entry string. Adds indentation for nesting, the formatted string, then a newline

        Parameters
        ----------
        depth : int
            Number of indents to prefix. Passed along to :func:`~TextReorder.build_depth`
        formatted_string : str
            A preformatted string to wrap with the appropriate whitespace for the settings file

        Returns
        -------
        str
            The preformatted string with indents and newline
            Ready to be appended to the output string
        """
        tabs = TextRecorder.build_depth(depth)
        return "{tabs}{formatted_string}\n".format(tabs=tabs, formatted_string=formatted_string)

    @staticmethod
    def write_stellaris_settings(filename: str, settings: List[Node], backup=True):
        """
        Builds the output string from the given settings then records it.

        Notes
        -----
        - If :data:`constants.ALTER_FILES` is disabled then the call to :func:`TextRecorder.record` will just print
          instead of writing to the file.

        Parameters
        ----------
        filename : str
            Name of the file to open and write to.
            If there is already a file matching the filename, it will be backed-up and overwritten
        settings : list[Node]
            The settings to write to the output

        Returns
        -------
        bool
            True if successful
        """
        output = TextRecorder.build_string_to_write_recursive(settings)
        TextRecorder.record(filename, output, backup)

    @staticmethod
    def build_string_to_write_recursive(data: List[Node], depth: int=0):
        """

        Parameters
        ----------
        data : list[Node]
            A list of :class:`~paradox_parser.node_object.Node` instances to write
        depth : int
            The number of tabs to print before a line.
            Used for nesting like in arrays. So if this is a child node it will have depth > 0
        """
        str_to_append = ""
        for node in data:
            if node.children:
                str_to_append += TextRecorder.build_string(depth, "{key}={{".format(key=node.key))
                str_to_append += TextRecorder.build_string_to_write_recursive(node.children, depth+1)
                str_to_append += TextRecorder.build_string(depth, "}")
            elif node.key and node.value:
                str_to_append += TextRecorder.build_string(
                    depth, "{key}={value}".format(key=node.key, value=node.value)
                )
            elif not node.key and node.value:
                str_to_append += TextRecorder.build_string(depth, node.value)
            elif node.value_type is value_types.EMPTY:
                str_to_append += "\n"
            else:
                print("wut?")
                exit(1)

        output = str_to_append
        return output





        



