import os
import shutil
import constants


class BaseRecorder:

    @staticmethod
    def get_absolute_path(filename):
        abspath = os.path.abspath(filename)
        return abspath

    @staticmethod
    def backup_file(filename):
        original = BaseRecorder.get_absolute_path(filename)
        backup = "{original}.backup".format(original=original)
        try:
            shutil.copy2(original, backup)
        except IOError:
            # If file doesn't exist then just return because there is nothing to backup
            return

    @staticmethod
    def record(filename, output_string, backup=True):
        """
        Writes a string to a file.

        Notes
        -----
        - If :data:`constants.ALTER_FILES` is disabled, then this function will print the output to the console.
          Used for testing.

        Parameters
        ----------
        filename : str
            The filename to write to
        output_string : str
            The string to write to the file
        backup : bool
            Set to ``False`` to disable backups

        """
        if constants.ALTER_FILES:
            if backup:
                BaseRecorder.backup_file(filename)
            file = open(filename, 'w+')
            file.write(output_string)
            file.close()
        else:
            print(output_string)

