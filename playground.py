from paradox_parser import reader
from profiles import database
import re

def main():
    test = reader.Reader("test_resources/test_settings.txt")
    test._hash_map['last_mods'].children.append(reader.Node("", '"HELLO/WORLD"'))
    pattern = re.compile("^(\t*)(.*)(\=)([\w\"\.]*)$", re.MULTILINE)
    file = open("test_resources/test_settings.txt", "r")

    weh = pattern.findall(file.read())

    return


def reading_file():
    file = open("test_resources/test_settings.txt", "r")
    print(file.readline())
    read_a_few_lines(file)


def read_a_few_lines(file_obj):
    while file_obj.readline():
        print(file_obj.readline())


def open_database():
    obj = database.Database()
    return obj


def play():
    # main()
    # reading_file()
    obj = open_database()
    return
