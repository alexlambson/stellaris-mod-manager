class NoMatchingValueType(Exception):
    """Raised when the stellaris type of a value given to Node cannot be determined

    Attributes:
        bad_value (str): The value given to Node
        message (str): Message to the programmer
    """
    def __init__(self, bad_value):
        self.bad_value = bad_value
        self.message = "\"{0}\" is not a valid Stellaris config type".format(bad_value)


class UnknownLineFormat(Exception):
    """Raised when a line in settings.txt does not match any known pattern

    Attributes:
        bad_line (str): The line that does not match a known stellaris config line
        line_number (int): The line number of the unknown format
        message (str): Message to the dev
    """
    def __init__(self, bad_line, line_number=None):
        self.bad_line = bad_line
        self.line_number = line_number
        if not line_number:
            self.message = "\"{0}\" is not a known Stellaris settings line format".format(bad_line)
        else:
            self.message = "\"{0}\" is not a known Stellaris settings line format\nLine:{1}".format(bad_line, line_number)
