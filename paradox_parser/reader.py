import re
import io
from paradox_parser.node_object import Node
from typing import List, Dict

NodeList = List[Node]
NodeHash = Dict[str, Node]


class Reader:
    @staticmethod
    def read_file(filename):
        file = open(filename, "r")
        settings = Reader.read_file_recurse(file, [])
        return settings

    @staticmethod
    def read_string_as_file(settings_string: str) -> List[Node]:
        fake_file = io.StringIO(settings_string)
        settings = Reader.read_file_recurse(fake_file, [])
        return settings

    @staticmethod
    def read_file_recurse(file, parsed_settings_container):
        line = "not empty"
        while line:
            line = file.readline()
            key_value_match = Reader.parse_key_value(line)
            if key_value_match:
                parsed_node = Node(key_value_match[0], key_value_match[1])
                parsed_settings_container.append(parsed_node)
                continue
            value_only = Reader.parse_value_only(line)
            if value_only:
                parsed_node = Node("", value_only[0])
                parsed_settings_container.append(parsed_node)
                continue
            beginning_of_collection_match = Reader.parse_beginning_of_collection(line)
            if beginning_of_collection_match:
                parsed_node = Node(beginning_of_collection_match[0], "")
                Reader.read_file_recurse(file, parsed_node.children)
                parsed_settings_container.append(parsed_node)
                continue
            if Reader.parse_is_ending_of_collection(line):
                return parsed_settings_container
        return parsed_settings_container

    @staticmethod
    def parse_key_value(line):
        pattern = re.compile("^(?:\t*)(.*)(?:=)([^\n{}]*)$")
        match = pattern.match(line)
        return match.groups() if match else False

    @staticmethod
    def parse_value_only(line):
        """
        Note, there should never be a value-only line as a root node
        Settings.txt only has value-only lines inside an array
        Args:
            line:

        Returns:

        """
        pattern = re.compile("^(?:\t*)([^{}=]+)$")
        match = pattern.match(line)
        return match.groups() if match else False

    @staticmethod
    def parse_beginning_of_collection(line):
        pattern = re.compile("^(?:\t*)(.*)(?:=)({)$")
        match = pattern.match(line)
        return match.groups() if match else False

    @staticmethod
    def parse_is_ending_of_collection(line) -> bool:
        return line.strip() == '}'

    @staticmethod
    def build_hash(settings, parent_key: str, hash_map):
        for node in settings:
            if node.key:
                hash_map[parent_key + node.key] = node
            if node.children:
                Reader.build_hash(node.children, node.key + '.', hash_map)
        return hash_map
