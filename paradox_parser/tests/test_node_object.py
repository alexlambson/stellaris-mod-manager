import pytest
from paradox_parser import node_object, value_types, exceptions


@pytest.mark.parametrize("input_value,expected", [
    ("50", False),
    ("", False),
    ("50.347aa", False),
    ("\"I am a string\"", True),
    ("\"67.8\"", True),
    ("\"broken string", False),
    ("identifier", False),
])
def test_is_string(input_value, expected):
    assert node_object.Node.is_string(input_value) == expected


@pytest.mark.parametrize("input_value,expected", [
    ("50.0000", True),
    ("", False),
    ("50.347aa", False),
    ("42", False),
    ("21.2", True),
])
def test_is_float(input_value, expected):
    assert node_object.Node.is_float(input_value) == expected


@pytest.mark.parametrize("input_value,expected", [
    ("50", True),
    ("", False),
    ("50.347aa", False),
    ("42", True),
    ("73.82", False),
])
def test_is_int(input_value, expected):
    assert node_object.Node.is_int(input_value) == expected


@pytest.mark.parametrize("input_value,expected", [
    ("50", False),
    ("", False),
    ("yes", True),
    ("no", True),
    ("true", False),
    ("0", False),
])
def test_is_boolean(input_value, expected):
    assert node_object.Node.is_boolean(input_value) == expected


@pytest.mark.parametrize("input_value,expected", [
    ("abc_hello", True),
    ("\"testing_am_string\"", False),
    ("50.347aa", False),
    ("identifier", True),
    ("identif13r", False),
    ("bad identifier", False),
])
def test_is_stellaris_identifier(input_value, expected):
    assert node_object.Node.is_stellaris_identifier(input_value) == expected


@pytest.mark.parametrize("input_value,processed_value,value_type", [
    ('"hello_world"', '"hello_world"', value_types.STRING),
    ("hello_world   ", "hello_world", value_types.STELLARIS_IDENTIFIER),
    ("50.55", "50.55", value_types.FLOAT),
    ("    50", "50", value_types.INT),
    ("\n", "", value_types.EMPTY),
])
def test_node_creation_success(input_value, processed_value, value_type):
    test_node = node_object.Node("junk_key", input_value)

    assert test_node.value_type == value_type
    assert test_node.key == "junk_key"
    assert test_node.value == processed_value


@pytest.mark.parametrize("input_value", [
    '"bad_string',
    'bad identifier',
    '50_009',
    '50.66_7',
    'bad_mod_path/forgot_quotes_for_string.mod',
])
def test_node_creation_failure(input_value):
    with pytest.raises(exceptions.NoMatchingValueType):
        node_object.Node("junk_key", input_value)
