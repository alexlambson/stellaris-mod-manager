import re

from paradox_parser import exceptions, value_types
from typing import List, Tuple, NewType


ValueType = NewType('ValueType', int)
"""
ValueType is a type for Node.value_type. 
It will store a constant from paradox_parser.value_types
"""


class Node:
    """Node Class

    This class represents a line in a Stellaris config file
    If this node represents the start of an array or dict, value will be None
    If this node represents an item in an array, key will be None

    Nodes are stored in a multi-dimensional linked-list.
    Each linked-list of nodes represents an group of settings. Arrays and dicts will be represented by a
    Node that has the parent_node attribute set to the key containing that list.

    Eg.

    ```
    setting=1
    x=4
    dicty={
        boy=4
        girl="mah hah"
    }
    turtles=no
    ```

    Would have a node map of:

    ```
    Node_list_1 => [setting, x, dicty, turtles]
    Node_list_2 => [boy, girl]
    Node_list_2.top.parent_node => Node_list_1[dicty]
    ```

    Notes:
        If key an value are empty strings then this node represents a blank line in order
        preserve blank lines in settings files,

    Attributes:
        key (str):
            This is the settings key.
            Will be an empty string if this Node is a value in an array
        value (str):
            The settings value.
            Will always be stored as a string.
            Will be empty if this node is the start of an array or dict
            How it will be written is determined by value_type
        value_type (ValueType):
            This attribute determines how the value will be written out.
            It is determined by what the value is in the settings file, eg:
                - "mod/ugc_17689" => string
                - 50.000000 => float
                - muh_key={"1","2","3"} => array
                - muh_key={nested="hi"} => dict
        children list[Node]:
            if the value is a dict or array, then self.children will hold that info
            Both are stored as a list of Node objects.
    """

    def __init__(self, key: str, raw_value: str):
        self.children: List[Node] = []
        self.key: str = key
        value, value_type = self.process_raw_value(raw_value)
        self.value: str = value
        self.value_type: ValueType = value_type

    def process_raw_value(self, raw_value: str) -> Tuple[str, ValueType]:
        """Process Raw Value

        Determines the type of and strips the value passed from paradox_parser.

        Notes:
            - This Does **not** parse a line from the config file, that is the parsers job

        Args:
            raw_value (str):
                A value from the paradox_parser, eg. if the config line is:
                    key="a value"
                then raw_value will be:
                    "a value"
                Please note that if the value is a string in the config, then quotes will be in the value

        Returns:
            str: The value stripped
            ValueType: The type of the value
        """
        processed_value = raw_value.strip()

        if not processed_value:
            return processed_value, value_types.EMPTY

        func_map = {
            value_types.STELLARIS_IDENTIFIER: self.is_stellaris_identifier,
            value_types.BOOLEAN: self.is_boolean,
            value_types.FLOAT: self.is_float,
            value_types.INT: self.is_int,
            value_types.STRING: self.is_string,
        }

        for value_type_key, type_check_function in func_map.items():
            if type_check_function(processed_value):
                value_type = value_type_key
                return processed_value, value_type

        raise exceptions.NoMatchingValueType(processed_value)

    @staticmethod
    def is_string(raw_value: str) -> bool:
        """Is String:
        This method assumes that the raw_value has already been stripped

        Args:
            raw_value (str): The raw string from the settings file. eg. everything after an "="

        Returns:
            bool: whether or not this is a string
        """
        return raw_value.startswith('"') and raw_value.endswith('"')

    @staticmethod
    def is_float(raw_value: str) -> bool:
        pattern = re.compile("^[0-9]+[.]+[0-9]+$")
        return True if pattern.match(raw_value) else False

    @staticmethod
    def is_int(raw_value: str) -> bool:
        pattern = re.compile("^[0-9]+$")
        return True if pattern.match(raw_value) else False

    @staticmethod
    def is_boolean(raw_value: str) -> bool:
        pattern = re.compile("^yes|no$")
        return True if pattern.match(raw_value) else False

    @staticmethod
    def is_stellaris_identifier(raw_value: str) -> bool:
        pattern = re.compile("^[a-zA-Z_0-9]+$")
        return True if pattern.match(raw_value) else False
