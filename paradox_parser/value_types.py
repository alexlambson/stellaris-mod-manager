STRING = 0  # strings begin and end with quotes. I have not seen escaped quotes in stellaris settings files, so I will assume they do not exist for now
FLOAT = 1
INT = 2
BOOLEAN = 3
STELLARIS_IDENTIFIER = 4  # stellaris identifiers look like a string, but don't have quotes
EMPTY = 5  # In order to not modify settings file, I need a way to keep track of blank lines
