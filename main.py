import constants
import textwrap
import cmd2
import math
import os
from profiles.steam import SteamUtils
from profiles.manager import ProfileManager
from helpers.directory import DirectoryHelper


class Prompt(cmd2.Cmd):
    profile_manager = None
    prompt = 'spm> '
    quit_on_sigint = True
    intro = textwrap.dedent(
        """
        Welcome to the Stellaris Profile Manager by Alex
        Currently only supports accepting an encoded mod string and activating those mods
        
        IMPORTANT NOTE: Right now I only store ONE backup of the settings, so if you save changes twice your original
        mod list will be lost unless you saved it as a profile.
        
        To easily share a modlist with a friend, call share_string
        
        Type "help" to list all commands
        """
    )

    def set_profile_manager(self, profile_manager: ProfileManager):
        self.profile_manager = profile_manager

    def do_share(self, _):
        if not self.profile_manager:
            raise AttributeError("Make sure to set the profile manager for the prompt")
        print(textwrap.dedent(
            """
            To share your mod list, copy the outputted string and send it to a friend
            Have them run the Stellaris Profile Manager on their machine and call

            activate <mod_share_string>

            Copy everything between the dotted lines:
            {dotted_line}""".format(dotted_line=constants.CMD_DOTTED_LINE)
        ))
        print(self.profile_manager.share_string().decode('ascii'))
        print(constants.CMD_DOTTED_LINE)

    def do_activate(self, compressed_mod_list: cmd2.Statement):
        print(textwrap.dedent(
            """
            Activated the following mods:
            {dots}
            """.format(dots=constants.CMD_DOTTED_LINE)
        ))
        compressed_mod_list = compressed_mod_list.arg_list[0]
        activated_mods = self.profile_manager.activate_from_share_string(compressed_mod_list)
        print(activated_mods)

    @staticmethod
    def do_exit():
        print(':\'(')
        return True

    def do_create_data_dir(self, _):
        self.profile_manager.create_user_data_directory()

    def do_profiles(self, _):
        print(textwrap.dedent(
            """
            These are the profiles I found, use their index to activate them:
            {dots}
            """.format(dots=constants.CMD_DOTTED_LINE)
        ))
        for profile_index, profile in enumerate(self.profile_manager.profiles):
            print("{index}: {name}".format(index=profile_index, name=profile.profile_name))

    def do_activate_profile(self, args: cmd2.Statement):
        index = int(args.arg_list[0])
        try:
            activated_profile = self.profile_manager.activate_profile(index)
            print(textwrap.dedent(
                """
                Activated "{profile}" with the following mods
                {dots}
                """.format(profile=activated_profile.profile_name, dots=constants.CMD_DOTTED_LINE)
            ))
            for node in activated_profile.hash_map[constants.SETTINGS_LAST_MODS_KEY].children:
                print(node.value)
        except IndexError:
            ordinal = "tsnrhtdd"[(math.floor(index/10) % 10 != 1) * (index % 10 < 4) * index % 10::4]
            print("There is no {}{} profile".format(index, ordinal))

    def do_backup(self, line):
        SteamUtils.create_local_copies_of_steam_mods()
        self.profile_manager.modify_last_mods_from_steam_to_local_copy()

    def do_write(self, _):
        self.profile_manager.save_changes()

    def default(self, line):
        if line == 'q':
            return True
        super(Prompt, self).default(line)


def main():
    if constants.TEST_MODE:
        filename = 'test_resources/test_settings.txt'
    else:
        filename = str(DirectoryHelper.get_stellaris_path('settings.txt'))

    profile_manager = ProfileManager(settings_filename=filename)

    prompt = Prompt()
    prompt.set_profile_manager(profile_manager)
    prompt.cmdloop()

    return


main()
