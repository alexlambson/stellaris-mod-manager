import pathlib
import sys


class DirectoryHelper:

    WINDOWS = 0
    MAC = 1
    LINUX = 2

    @staticmethod
    def _get_mac_stellaris_folder():
        return pathlib.Path('~/Documents/Paradox Interactive/Stellaris/').expanduser()

    @staticmethod
    def _get_windows_stellaris_folder():
        if DirectoryHelper.get_platform() is not DirectoryHelper.WINDOWS:
            return None

        import ctypes
        from ctypes.wintypes import MAX_PATH

        dll = ctypes.windll.shell32
        buf = ctypes.create_unicode_buffer(MAX_PATH + 1)
        if dll.SHGetSpecialFolderPathW(None, buf, 0x0005, False):
            documents = buf.value.replace('\\', '/')
            return pathlib.Path(documents) / 'Paradox Interactive/Stellaris/'
        else:
            raise EnvironmentError('Could not find "My Documents"')

    @staticmethod
    def get_platform() -> int:
        platforms = {
            'win32': DirectoryHelper.WINDOWS,
            'darwin': DirectoryHelper.MAC,
            # 'linux1': DirectoryHelper.LINUX,
            # 'linux2': DirectoryHelper.LINUX,
        }
        if sys.platform not in platforms:
            raise NotImplemented('"{}" is an unsupported OS')

        return platforms[sys.platform]

    @staticmethod
    def _choose_os_path() -> pathlib.Path:
        paths = {
            DirectoryHelper.WINDOWS: DirectoryHelper._get_windows_stellaris_folder(),
            DirectoryHelper.MAC: DirectoryHelper._get_mac_stellaris_folder(),
        }
        user_platform = DirectoryHelper.get_platform()
        path = paths[user_platform]
        return path

    @staticmethod
    def get_stellaris_path(*argv) -> pathlib.Path:
        built_path = DirectoryHelper._choose_os_path()
        for arg in argv:
            built_path = built_path / arg
        return built_path



