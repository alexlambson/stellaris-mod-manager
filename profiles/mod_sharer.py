import zlib
import base64

import constants

from profiles import manager
from paradox_parser import reader, node_object


class ModSharer:
    def __init__(self, profile_manager: manager.ProfileManager):
        """
        Initialize the mod sharer with a profile manager reference.

        Note that ``profile_manager`` will be modified if calling :func:`ModSharer.activate_from_share_string`

        Parameters
        ----------
        profile_manager: manager.ProfileManager
            The manager to be read and written to
        """
        self.profile_manager: manager.ProfileManager = profile_manager

    @staticmethod
    def compress_mod_list(mod_list_string: str) -> bytes:
        """
        Encodes a list of mods to be shared with a friend

        Parameters
        ----------
        mod_list_string : str
            A str of mod filenames eg ``ugc_000001.mod`` seperated by newlines

        Returns
        -------
        bytes
        """
        compressed = zlib.compress(mod_list_string.encode('ascii'), level=9)
        b64_encoded = base64.b64encode(compressed)
        return b64_encoded

    @staticmethod
    def decompress_mod_list(mod_list_compressed: str) -> str:
        b64_decoded = base64.b64decode(mod_list_compressed)
        return zlib.decompress(b64_decoded).decode('ascii')

    def share_string(self) -> bytes:
        self.profile_manager.reload()
        mods_dict: node_object.Node = self.profile_manager._hash_map[constants.SETTINGS_LAST_MODS_KEY]
        mods_string = ""
        for mod_node in mods_dict.children:
            mods_string += "{mod_file_name}\n".format(mod_file_name=mod_node.value)
        mods_string = self.compress_mod_list(mods_string)
        return mods_string

    def activate_from_share_string(self, compressed_mod_list: str):
        """
        Activates the mods from a share string.
        Share strings are b64encoded and compressed mod lists.

        This function decodes and decompresses the mod string, creates a list of nodes from that string,
        saves that list to the in-memory settings, then writes the settings

        Parameters
        ----------
        compressed_mod_list : str
            zlib compressed and b64 encoded mod list

        Returns
        -------
        str
            The decompressed and decoded mod list to print to the console.
        """
        decoded_string = self.decompress_mod_list(mod_list_compressed=compressed_mod_list)
        shared_settings = reader.Reader.read_string_as_file(decoded_string)
        self.profile_manager._hash_map[constants.SETTINGS_LAST_MODS_KEY].children = shared_settings
        self.profile_manager.save_changes()
        return decoded_string


