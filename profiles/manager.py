import constants
import os
import sys
import glob
import zlib
import base64
from profiles.steam import WORKSHOP_BACKUP_PREFIX
from typing import List, Dict
from paradox_parser import reader, node_object
from profiles import config_types
from recorder import text_recorder


class ProfileManager:
    """
    Attributes
    ----------
    settings_filename : str
        The filename for the settings to read / write to.
    settings : list[parser.node_object.Node]
        The array of parsed settings represented as :class:`~reader.node_object.Node` instances
    _hash_map : dict[str, node_object.Node]
        A dict of keys pointing to nodes in the ``settings`` array. These are references, so modifying something
        in ``_hash_map`` will modify it in ``settings``.
        This was made to make it easy to access settings.
        Instead of needing to loop over ``settings`` to find 'last_mods', just do ``_hash_map['last_mods']``
    """

    def __init__(self, settings_filename):
        if not (os.path.exists(settings_filename) and os.path.isfile(settings_filename)):
            print("The file `{filename}` does not exist".format(filename=settings_filename))
            sys.exit(constants.SETTINGS_FILE_NOT_FOUND)

        ProfileManager.create_user_data_directory()

        self.settings_filename: str = settings_filename
        self._update_memory_settings()

        self.project_recorder = text_recorder.TextRecorder()

        self.profiles: List[config_types.Profile] = self.read_profiles()

    def _update_memory_settings(self):
        """
        Reads the Stellaris settings file and updates :data:`~ProfileManager.settings` and
        :data:`~ProfileManager._hash_map`
        """
        self.settings: List[node_object.Node] = reader.Reader.read_file(self.settings_filename)

        self._hash_map: Dict[str, node_object.Node] = reader.Reader.build_hash(self.settings, '', {})

    def reload(self):
        """
        Reloads info from disk.

        Calls :func:`ProfileManager._update_memory_settings`
        """
        self._update_memory_settings()
        return self

    @staticmethod
    def scan_user_data_directory() -> List[str]:
        """
        Scans the Stellaris Profile Manager Data folder.

        Returns a list of filepaths to SPM profiles

        Returns
        -------
        list[str]
            List of profile paths
        """
        user_dir = constants.USER_DATA_PATH if not constants.TEST_MODE else constants.TEST_USER_DATA_PATH
        return glob.glob(
            "{user_dir}/*{suffix}".format(
                user_dir=user_dir, suffix=constants.PROFILE_SUFFIX
            )
        )

    def read_profiles(self) -> List[config_types.Profile]:
        """
        Reads all user profiles from the SPM user data dir and stores them as :class:`config_types.Profile` objects

        Returns
        -------
        list[config_types.Profile]
        """
        profile_filenames = self.scan_user_data_directory()
        profiles = []
        for profile_filename in profile_filenames:
            profile = config_types.Profile(profile_filename)
            profiles.append(profile)
        return profiles

    def get_hash(self, key) -> node_object.Node:
        """
        Get a key from the settings hash table.

        Parameters
        ----------
        key : str
            The key to attempt to get

        Returns
        -------
        node_object.Node
        """
        return self._hash_map.get(key, None)

    @staticmethod
    def compress_mod_list(mod_list_string: str) -> bytes:
        """
        Encodes a list of mods to be shared with a friend

        Parameters
        ----------
        mod_list_string : str
            A str of mod filenames eg ``ugc_000001.mod`` seperated by newlines

        Returns
        -------
        bytes
        """
        compressed = zlib.compress(mod_list_string.encode('ascii'), level=9)
        b64_encoded = base64.b64encode(compressed)
        return b64_encoded

    @staticmethod
    def decompress_mod_list(mod_list_compressed: str) -> str:
        b64_decoded = base64.b64decode(mod_list_compressed)
        return zlib.decompress(b64_decoded).decode('ascii')

    def share_string(self) -> bytes:
        self._update_memory_settings()
        mods_dict: node_object.Node = self._hash_map[constants.SETTINGS_LAST_MODS_KEY]
        mods_string = ""
        for mod_node in mods_dict.children:
            mods_string += "{mod_file_name}\n".format(mod_file_name=mod_node.value)
        mods_string = self.compress_mod_list(mods_string)
        return mods_string

    def activate_from_share_string(self, compressed_mod_list: str):
        """
        Activates the mods from a share string.
        Share strings are b64encoded and compressed mod lists.

        This function decodes and decompresses the mod string, creates a list of nodes from that string,
        saves that list to the in-memory settings, then writes the settings

        Parameters
        ----------
        compressed_mod_list : str
            zlib compressed and b64 encoded mod list

        Returns
        -------
        str
            The decompressed and decoded mod list to print to the console.
        """
        decoded_string = self.decompress_mod_list(mod_list_compressed=compressed_mod_list)
        shared_settings = reader.Reader.read_string_as_file(decoded_string)
        self._hash_map[constants.SETTINGS_LAST_MODS_KEY].children = shared_settings
        self.save_changes()
        return decoded_string

    @staticmethod
    def create_user_data_directory():
        """
        Create the user directory for SPM.
        It's where the mod profiles are stored
        """
        os.makedirs(constants.USER_DATA_PATH, exist_ok=True)

    def modify_last_mods_from_steam_to_local_copy(self):
        # this is really hacky and needs to be fixed once I have better string management
        last_mods = self._hash_map.get('last_mods', None)
        if not last_mods:
            return
        for node in last_mods.children:
            if node.value.startswith('"mod/LC_'):
                continue
            repd = node.value.replace('"', '')
            repd = repd.replace('mod/', '')
            repd = 'mod/' + WORKSHOP_BACKUP_PREFIX + repd
            final = '"{}"'.format(repd)
            node.value = final
        self.save_changes()

    def save_changes(self):
        """
        Saves the in-memory to the Stellaris settings file
        """
        self.project_recorder.write_stellaris_settings(self.settings_filename, self.settings)

    def activate_profile(self, index: int) -> config_types.Profile:
        profile = self.profiles[index]
        profile_mods = profile.hash_map[constants.SETTINGS_LAST_MODS_KEY].children
        self._hash_map[constants.SETTINGS_LAST_MODS_KEY].children = profile_mods
        self.save_changes()
        return profile

