import sqlite3
import constants
from sqlite3 import Error


class Database:

    def __init__(self):
        self._db_connection = self._get_connection(constants.PROFILE_DATABASE_PATH)

    @staticmethod
    def _get_new_connection(db_file):
        """ create a database connection to a SQLite database """
        try:
            conn = sqlite3.connect(db_file)
            print(sqlite3.version)
        except Error as e:
            print(e)
            raise

        return conn

    def close_connection(self):
        self._db_connection.close()
