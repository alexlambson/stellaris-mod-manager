import os
import re
import shutil
import pathlib
from helpers.directory import DirectoryHelper
from recorder import text_recorder
from profiles import config_types


# Steam workshop consts
STELLARIS_GAME_ID = 281990
WORKSHOP_BACKUP_LOCATION = 'mod/steam_backup/'
WORKSHOP_BACKUP_PREFIX = 'LC_'

STELLARIS_FOLDER_MAC = os.path.expanduser('~/Documents/Paradox Interactive/Stellaris/')


class SteamUtils:
    @staticmethod
    def _find_steam_workshop_mods(mods_folder: pathlib.Path) -> config_types.ModList:
        filename_pattern = re.compile("(?:ugc_)(\d+)(?:\.mod)")
        workshop_mods: config_types.ModList = []

        for filename in os.listdir(str(mods_folder)):
            if filename_pattern.match(filename):
                file_path = pathlib.Path(mods_folder, filename)
                mod = config_types.ModConfig(str(file_path))
                workshop_mods.append(mod)

        return workshop_mods

    @staticmethod
    def _rename_mods_and_modify_configs(mods_folder: pathlib.Path, mod_list: config_types.ModList):
        backup_folder_path = DirectoryHelper.get_stellaris_path(WORKSHOP_BACKUP_LOCATION)
        os.makedirs(str(backup_folder_path), exist_ok=True)
        for mod in mod_list:
            config_filename = os.path.split(mod.filename)[1]
            backup_config_path = DirectoryHelper.get_stellaris_path(
                mods_folder, WORKSHOP_BACKUP_PREFIX + config_filename
            )
            try:
                original_name = mod.name
                backup_mod_location = shutil.copy2(mod.mod_location.replace('"', ''), str(backup_folder_path))
                mod.filename = backup_config_path
                mod.mod_location = '"{}"'.format(backup_mod_location)
                mod.name = '"!LC {}"'.format(mod.name.replace('"', ''))
                mod.update_settings()
                mod.delete_steam_id_for_local_backup()
                text_recorder.TextRecorder.write_stellaris_settings(mod.filename, mod.settings, backup=False)
                print(
                    'Backed up {original} to {new_name}'.format(
                        original=original_name, new_name=mod.name
                    )
                )
            except IOError as e:
                print("Failed to backup: config={config}, mod_name={mod_name}, archive={archive}".format(
                    config=mod.filename,
                    mod_name=mod.name,
                    archive=mod.mod_location,
                ))

    @staticmethod
    def create_local_copies_of_steam_mods():
        mods_folder = DirectoryHelper.get_stellaris_path('mod/')
        mod_list = SteamUtils._find_steam_workshop_mods(mods_folder)
        SteamUtils._rename_mods_and_modify_configs(mods_folder, mod_list)
        return mod_list
