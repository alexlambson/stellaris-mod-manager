import constants
from typing import List
from paradox_parser.reader import Reader, NodeList, NodeHash

ModList = List['ModConfig']


class ParadoxConfigBase:
    """
    Attributes
    ----------
    filename : str
        The filename of the profile
    settings : list[parser.node_object.Node]
        The settings stored in the profile
    hash_map : dict[str, parser.node_object.Node]
        Similar to the hash map in :class:`~profiles.manager.ProfileManager`. Stores easy to access references to
        :data:`~Profile.settings`
    """
    def __init__(self, filename):
        self.filename: str = filename
        self.settings: NodeList = Reader.read_file(filename)
        self.hash_map: NodeHash = Reader.build_hash(self.settings, '', {})

    def update_hash(self):
        self.hash_map = Reader.build_hash(self.settings, '', {})


class Profile(ParadoxConfigBase):
    """
    Attributes
    ----------
    profile_name : str
        The name of the profile extracted from the file. See :data:`constants.PROFILE_NAME_SETTINGS_KEY` for
        string that is searched for
    """
    def __init__(self, filename):
        super().__init__(filename)

        self.profile_name: str = self.hash_map[constants.PROFILE_NAME_SETTINGS_KEY].value


class ModConfig(ParadoxConfigBase):
    def __init__(self, filename):
        super().__init__(filename)

        self.name: str = self.hash_map[constants.MOD_NAME_CONFIG_KEY].value
        self.mod_location: str = self.hash_map[constants.MOD_LOCATION_CONFIG_KEY].value
        self.steam_id: str = self.hash_map[constants.MOD_STEAM_ID_CONFIG_KEY].value

    def update_settings(self):
        self.hash_map[constants.MOD_NAME_CONFIG_KEY].value = self.name
        self.hash_map[constants.MOD_LOCATION_CONFIG_KEY].value = self.mod_location
        self.hash_map[constants.MOD_STEAM_ID_CONFIG_KEY].value = self.steam_id
        self.update_hash()

    def delete_steam_id_for_local_backup(self):
        self.settings.remove(self.hash_map[constants.MOD_STEAM_ID_CONFIG_KEY])
        self.steam_id = None
        self.update_hash()


