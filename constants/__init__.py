import os

TEST_MODE = 1
TEST_LAST_MODS = '"i_am_mod.mod"\n"metoo.mod"\n"methree.mod"\n'

USER_DATA_PATH = "stellaris_profile_manager_user_data"
TEST_USER_DATA_PATH = "test_resources/user_data"
PROFILE_SUFFIX = "_spm_profile.txt"

PROFILE_DATABASE_FILENAME = "profiles.db"
PROFILE_DATABASE_PATH = os.path.join(USER_DATA_PATH, PROFILE_DATABASE_FILENAME)
PROFILE_NAME_SETTINGS_KEY = 'name'

# Mod config file keys
MOD_NAME_CONFIG_KEY = 'name'
MOD_STEAM_ID_CONFIG_KEY = 'remote_file_id'
MOD_LOCATION_CONFIG_KEY = 'archive'

SETTINGS_LAST_MODS_KEY = 'last_mods'

SETTINGS_FILE_NOT_FOUND = 10

ALTER_FILES = False

CMD_DOTTED_LINE = "----------------------------"
